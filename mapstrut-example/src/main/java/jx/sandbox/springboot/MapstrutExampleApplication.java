package jx.sandbox.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MapstrutExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(MapstrutExampleApplication.class, args);
	}

}
